===========================================
About me
===========================================

As someone who dreads coding, this is my "Conquer your fear moments". This is what drives me to pursue learning in this field.

I have a brief experience in the BPO industry as a Technical Support Representative and as production assistant for a small scale mineral processing company.

I hope that everyone will be able to finished this bootcamp and be successful in their future endeavors.

mic-drop
===========================================